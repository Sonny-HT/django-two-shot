from django.urls import path
from accounts.views import get_login_page, logout_user, signup

urlpatterns = [
    path("login/", get_login_page, name="login"),
    path("logout/", logout_user, name="logout"),
    path("signup/", signup, name="signup"),
]
