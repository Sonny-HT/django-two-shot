from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.models import User
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


def show_receipts(request):
    if not request.user.is_authenticated:
        return redirect("login")

    receipts = Receipt.objects.filter(
        purchaser__username=request.user.username
    )
    context = {"receipts": receipts}
    return render(request, "receipts-home.html", context)


def create_receipt(request):
    if not request.user.is_authenticated:
        return redirect("login")
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form}
    return render(request, "create-receipt.html", context)


def category_list(request):
    if not request.user.is_authenticated:
        return redirect("login")
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"categories": categories}
    return render(request, "show-categories.html", context)


def account_list(request):
    if not request.user.is_authenticated:
        return redirect("login")
    accounts = Account.objects.filter(owner=request.user)
    context = {"accounts": accounts}
    return render(request, "show-accounts.html", context)


def create_category(request):
    if not request.user.is_authenticated:
        return redirect("login")
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(commit=False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "create-category.html", context)


def create_account(request):
    if not request.user.is_authenticated:
        return redirect("login")
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm
    context = {"form": form}
    return render(request, "create-account.html", context)
